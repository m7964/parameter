package parameter;

public class Application {
	
	public static void main(String[] args) {
		
		
		
		/* 메소드의 파라미터 선언부에는 다양한 종류의 값을 인자로 호출할 수 있다.
		 * 
		 *  매개변수(parameter)로 사용 가능한 자료형
		 *  1. 기본자료형
		 *  2. 기본자료형 배열
		 *  3. 클래스자료형
		 *  4. 클래스자료형 배열
		 *  5. 가변인자
		 *  */
		
		/* 1. 기본 자료형을 매개변수로 전달 받는 메소드 호출 확인
		 * 기본자료형 8가지 모두 가능하다.*/
		
		
		/* 파라미터 타입별 호출 테스트 기능을 제공하는 ParameterTest 클래스  */
		
		ParameterTest pt = new ParameterTest();
		int num = 20;
		pt.testPrimaryTypeParameter(num);
		
		
		/* 2.  기본 자료형 배열을 매개변수로 전달 받는 메소드 호출 확인 */
		int[] iarr = new int[] {1,2,3,4,5};
		
		pt.testPrimaryTypeArrayParameter(iarr);
		
		/* 3. 클래스 자료형 */
		
		RectAngle r1 = new RectAngle(12.5, 22.5);
		
		System.out.println("인자로 전달하는 값 : " + r1);
		pt.testClassTypeParameter(r1);
		
		System.out.println("===================== 변경후 사각형의 넓이와 둘레 =============================");
		r1.calcArea();
		r1.calcRound();
		
		
		
		/* 4. 클래스 자료형 배열 */
		
		
		/* 5. 가변인자 
		 *  인자로 전달하는 값의 갯수가 정해지지 않은 경우에 가변배열을 활용한다.
		 * */
		
		pt.testVariableLengthArrayParameter("홍길동");
		pt.testVariableLengthArrayParameter("유관순", "볼링");
		pt.testVariableLengthArrayParameter("이순신", "볼링","축구","당구");
		pt.testVariableLengthArrayParameter("이순실", new String[] {"테니스","서예","떡썰기"}); // 배열도 가능
	}

}








































